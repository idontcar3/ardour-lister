#! /usr/bin/env python3.7 
# -*- encoding: utf-8 -*-
import os

from ardour_lister import *
from assets import css, js
from flask import Flask
from flask_assets import Environment



def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    # assets
    assets = Environment(app)

    assets.register('js_all', js)
    assets.register('css_all', css)

    js.build()
    css.build()

    # other config
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'ardour-lister.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    app.register_blueprint(bp)

    return app

if __name__ == '__main__':
    app = create_app()
    app.run()