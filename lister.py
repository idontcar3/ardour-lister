#! /usr/bin/env python3.7 
# -*- encoding: utf-8 -*-
import os

from datetime import datetime
from lxml import etree

test_filter = [
    ".bak",
    ".cue", ".flac", ".toc",
    ".txt"
]


def stamp(unix_time):
    return datetime.fromtimestamp(unix_time).isoformat()


def path_to_dict(path):
    # d = {'name': os.path.basename(path)}
    d = {'text': os.path.basename(path)}
    if os.path.isdir(path):
        d['type'] = "directory"
        d["icon"] = "fas fa-folder normal-folder"
        childrens = list()

        had_session = False
        for x in os.listdir(path):

            have_session = False
            data = path_to_dict(os.path.join(path, x))

            if x.endswith(".ardour"):
                had_session = True
                data["icon"] = "fas fa-file session-file"

            if data:
                childrens.append(data)

        d['children'] = childrens

        if had_session:
                d["icon"] = "fas fa-folder session-folder"

    else:
        d['type'] = "file"
        ext = os.path.splitext(d["text"])[1]
        if ext not in test_filter and ".ardour" in ext:
            # ardour session
            d = {**parse_ardour_file(path), **d}
            return d
        return None
    return d


def list_sessions(ARDOUR_SESSIONS):
    """List ardour sessions from a given location
    Fetch the songs in the export folder
    Make a mosaic from the making folder
    """
    ardour_files = list()

    last_parent = ARDOUR_SESSIONS
    for dirpath, dirnames, filenames in os.walk(ARDOUR_SESSIONS):

        if dirnames:
            # have children see if ardour dir
            # if ardour dir, get data from dirs
            # else this can be an arrangement folder
            # need to children it
            pass

        if last_parent != dirpath:
            last_parent = dirpath
            print(last_parent)
        path = dirpath.split(os.sep)

        # print((len(path) - 1) * '---', os.path.basename(dirpath))
        for fname in filenames:

            # skip bakup files
            if ".bak" in fname:
                continue

            # get only ardour files
            if ".ardour" in fname:
                print(len(path) * '---', fname)
                fpath = os.path.join(dirpath, fname)

                # TODO:
                # parse the file and get some params
                # found ardour file
                session = parse_ardour_file(fpath)

                session["dir"] = os.path.dirname(fpath).split(ARDOUR_SESSIONS)[1]
                ardour_files.append(session)

    ardour_files.sort(key=lambda x: x["created"])
    return ardour_files


def parse_ardour_file(fpath):
    """Read/Parse an ardour file given the location"""
    # print("parsing: ", fpath)
    stat = os.stat(fpath)

    with open(fpath, "r", encoding="utf-8") as fp:
        content = bytes(fp.read(), encoding="utf-8")

    tree = etree.fromstring(content)

    ardour = tree.xpath("//ProgramVersion")[0].get("created-with")

    # get the exports content
    dirname = os.path.dirname(fpath)
    exports = os.listdir(os.path.join(dirname, "export"))

    files = list()
    for e in exports:
        if "." + e.split(".")[-1] not in test_filter:
            files.append(e)

    # making_folder = os.path.join(dirname, "making")
    screenshots_folder = os.path.join(dirname, "screenshots")

    if os.path.isdir(screenshots_folder):
        screenshots = os.listdir(screenshots_folder)
    else:
        screenshots = list()
        os.makedirs(screenshots_folder)

    _file = fpath.split(os.path.sep)[-1].split(".ardour")[0]
    return {
        "ardour": int(ardour.split("Ardour ")[1].split(".")[0]),
        "version": ardour,
        "path": fpath,
        "file": _file,
        "text": _file,
        "dir": dirname,
        # "stat": stat,
        "created": stamp(stat.st_ctime),
        "modified": stamp(stat.st_mtime),
        "accesed": stamp(stat.st_atime),
        "exports": files,
        "screenshots": screenshots,
        # "text": dirname.split(os.path.sep)[-1],
    }
