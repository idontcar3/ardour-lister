# Ardour Lister

This program helps me list Ardour sessions in a nicer way.
I do a lot of experiments.

Uses flask for the interface.

## TODO
* create a javascript function to handle the play
* check what's going on with the wav files.
* make it run the sessions
* enhance ardour binaries location
* display settings on the site
* add the description to the list item view
* number of tracks and such?
* stats and session open count
* refresh icon next to files
* tabs for files/stats
* folder tree events.
* create folder/files
* filter by ardour version and path
* make the folders compact when the others expand

### For later
* Make a sexy theme :p.