from flask_assets import Bundle


js = Bundle(
	'js/jquery-3.3.1.min.js',
    'js/bootstrap.min.js', 'js/fontawesome.min.js', 'js/jstree.min.js',
    'js/list.min.js', 'js/popper.min.js',
    'js/solid.min.js', 'js/script.js',
    filters='jsmin', output='gen/packed.js'
)

css = Bundle(
    'css/bootstrap.min.css', 'css/fontawesome.min.css',
    'css/responsive.css', 'css/style.css', 'css/file-tree-style.min.css',
    filters='cssmin', output='gen/packed.css'
)
