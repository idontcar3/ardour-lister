#! /usr/bin/env python3.7
# -*- encoding: utf-8 -*-
import os
from flask import Blueprint, render_template, request, send_from_directory
from subprocess import Popen

from lister import path_to_dict

# TODO:
# Locate these in a system-dependant fashion
# Maybe with the icons
ardour5 = "/usr/local/bin/ardour5"
ardour6 = "/usr/local/bin/ardour6"

# TODO: ask for the user for this input
ARDOUR_SESSIONS = "/home/dunno/ardour"
CUR_DIR = os.path.dirname(os.path.realpath(__file__))


beta = path_to_dict(ARDOUR_SESSIONS)

ARDOUR_FILES = [
    {
        "text": ARDOUR_SESSIONS,
        "children": beta,
        "state": "opened"
    }
]

bp = Blueprint('index', __name__, url_prefix='/', template_folder='templates')


@bp.route('/sessions/<path:filename>')
def session_static(filename):
    return send_from_directory("/", filename)


@bp.route("/", methods=('GET',))
def index():
    global ARDOUR_FILES

    return render_template('index.html', ardour_files=ARDOUR_FILES)


@bp.route("/run", methods=('POST', ))
def run_ardour():
    fname = request.forms.get("fname")
    version = request.forms.get("version")

    print("running")
    print(version)

    if version == '5':
        # run ardour 5
        ardour_command = ardour5
    else:
        # run ardour 6
        ardour_command = ardour6

    command = [ardour_command, fname]
    print(command)
    print(Popen(command))

    return {"status": "ok"}